<?php

/**
 * @package ccPlatformSh
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    01 Sep 2017
 * */

require 'autoload.php';
$cli = eZCLI::instance();
$cli->setUseStyles( true );

$scriptSettings                   = array();
$scriptSettings['description']    = 'Overrides eZINI php class';
$scriptSettings['use-extensions'] = false;
$scriptSettings['use-session']    = false;
$scriptSettings['use-modules']    = false;

$script  = eZScript::instance($scriptSettings);
$script->startup();
// We are not calling initialize because we might not have correct cluster settings at this point
//$script->initialize();

$origPath = 'lib/ezutils/classes/ezini.php';
$destPath = 'extension/cc_platformsh/classes/ezini.php';

$cli->output('Copying ' . $origPath . ' to ' . $destPath);
if (copy($origPath, $destPath) === false) {
	$cli->output('Failed. Please check file permissions.');
	$script->shutdown(1);
}

$content = file_get_contents($destPath);

$changedPath    = $destPath . '.changed';
$changedContent = str_replace("<?php\n", "<?php\nnamespace custom;\n", $content);
file_put_contents($changedPath, $changedContent);
require $changedPath;
$reflection  = new ReflectionClass('\\custom\\eZINI');

$staticProps     = $reflection->getStaticProperties();
$useIniInjection = array_key_exists('injectedSettings', $staticProps);

if( $useIniInjection ) {
	$methodToModify = 'eZINI';
	$appendCode     = '
        $platformSettings       = ccPlatformShSettings::getInstance()->getSettings();
        foreach ($platformSettings as $file => $fileSettings) {
            foreach ($fileSettings as $block => $settings) {
                foreach ($settings as $setting => $value) {
                    self::$injectedSettings[$file][$block][$setting] = $value;
                }
            }
        }';
} else {
	$methodToModify = 'parse';
	$appendCode     = '
        $platformSettings = ccPlatformShSettings::getInstance()->getSettings();
        if (array_key_exists($this->FileName, $platformSettings))
        {
            foreach ($platformSettings[$this->FileName] as $block => $settings) {
                foreach ($settings as $setting => $value) {
                    $this->BlockValues[$block][$setting] = $value;
                }
            }
        }';

}
$cli->output('injectedSettings are in use: ' . ($useIniInjection ? 'yes' : 'no'));

$cli->output('Modifying content of eZINI file ...');
$method  = $reflection->getMethod($methodToModify);
$content = file($destPath);
$endLine = $method->getEndLine() - 2;
$content[$endLine] = $appendCode . PHP_EOL . $content[$endLine];
$content = implode('', $content);
file_put_contents($destPath, $content);
unlink($changedPath);

$cli->output('Done');
$script->shutdown( 0 );