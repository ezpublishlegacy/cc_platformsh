cc_platformsh
-------------------
It is ezpublish legacy extension, which makes platform.sh integration easier. It should
be used **ONLY** on legacy installations (without symfony stack).

Requirements
------------
We are assuming `.platform/services.yaml` is similar to:
```
mysqldb:
    type: mysql:10.0
    disk: 768
    configuration:
        schemas:
            - main
            - cluster
        endpoints:
            mysql:
                default_schema: main
                privileges:
                    main: admin
                    cluster: admin
```
And there are following relations in `.platform.app.yaml`:
```
relationships:
    database: "mysqldb:mysql"
```

Installation
------------
1. Enabled cc_platformsh extension
2. Set `EZP_AUTOLOAD_ALLOW_KERNEL_OVERRIDE` to `true` in `config.php`
3. If clustering is not enabled, you could skip this step. Otherwise please add following code in `config.php`
    ```
    require_once "extension/cc_platformsh/classes/platformsh_settings.php";
    $dbSettings = ccPlatformShSettings::getInstance()->getDatabaseSettings();

    define( 'CLUSTER_STORAGE_DB', 'cluster' );
    define( 'CLUSTER_STORAGE_HOST', $dbSettings['host'] );
    define( 'CLUSTER_STORAGE_PORT', $dbSettings['port'] );
    define( 'CLUSTER_STORAGE_USER', $dbSettings['user'] );
    define( 'CLUSTER_STORAGE_PASS', $dbSettings['pass'] );
    ```
4. Setup following platformsh hooks:
    ```
    hooks:
        # Build hooks can modify the application files on disk but not access any services like databases.
        build: |
            if [ -x "extension/cc_platformsh/bin/php/init_ezini.php" ]; then
                php extension/cc_platformsh/bin/php/init_ezini.php
            fi
    
        # Deploy hooks can access services but the file system is now read-only.
        deploy: |
            if [ -x "bin/php/ezpgenerateautoloads.php" ]; then
                php bin/php/ezpgenerateautoloads.php -o
                php bin/php/ezpgenerateautoloads.php
            fi
    ```
5. Deploy changes to platform.sh.