<?php

/**
 * @package ccPlatformSh
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    23 Aug 2017
 */

class ccPlatformShSettings {

    /**
     * A base64-encoded JSON object whose keys are the relationship name
     * and the values are arrays of relationship endpoint definitions.
     *
     * @var string
     */
    const RELATIONSHIPS = 'PLATFORM_RELATIONSHIPS';

    /**
     * A base64-encoded JSON object which keys are variables names and
     * values are variable values.
     *
     * @var string
     */
    const VARIABLES = 'PLATFORM_VARIABLES';

    /**
     * Main DB service endpoint
     *
     * @var string
     */
    const ENDPOINT_DB = 'database';

    /**
     * SOLR service endpoint
     *
     * @var string
     */
    const ENDPOINT_SOLR = 'solr';

    /**
     * @var ccPlatformShSettings
     */
    private static $instance = null;

    /**
     * Singleton
     *
     * @return ccPlatformShSettings
     */
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Decodes value from relationships env variable
     *
     * @return array()
     */
    protected function getRelationships() {
        $tmp = getenv(self::RELATIONSHIPS);
        return json_decode(base64_decode($tmp), true);
    }

    /**
     * Decodes value from env variables
     *
     * @return array()
     */
    protected function getVariables() {
        $tmp = getenv(self::VARIABLES);
        return json_decode(base64_decode($tmp), true);
    }

    /**
     * Extracts settings from env variable
     *
     * @return array()
     */
    public function getSettings() {
        $return = array();

        $dbSettings = $this->getDatabaseSettings();
        if ($dbSettings !== null) {
            $return['site.ini'] = array(
                'DatabaseSettings' => array(
                    'Database' => $dbSettings['name'],
                    'User'     => $dbSettings['user'],
                    'Password' => $dbSettings['pass'],
                    'Server'   => $dbSettings['host'],
                    'Port'     => $dbSettings['port']
                )
            );
            $return['file.ini'] = array(
                'eZDFSClusteringSettings' => array(
                    'DBName'     => 'cluster',
                    'DBUser'     => $dbSettings['user'],
                    'DBPassword' => $dbSettings['pass'],
                    'DBHost'     => $dbSettings['host'],
                    'DBPort'     => $dbSettings['port']
                )
            );
        }

        return $return;
    }

    /**
     * Extracts database settings. We are assuming, that platform.sh cluster database name is "cluster".
     * And it is using the same endpoint as main database. So database service definition is similar to:
     * 
     * mysqldb:
     *     type: mysql:10.0
     *     disk: 768
     *     configuration:
     *         schemas:
     *             - main
     *             - cluster
     *         endpoints:
     *             mysql:
     *                 default_schema: main
     *                 privileges:
     *                     main: admin
     *                     cluster: admin
     *
     * @return array()
     */
    public function getDatabaseSettings() {
        $relationships = $this->getRelationships();
        if (is_array($relationships) === false || isset($relationships[self::ENDPOINT_DB]) === false) {
            return null;
        }

        $endpoint = $relationships[self::ENDPOINT_DB][0];
        return array(
            'name' => $endpoint['path'],
            'user' => $endpoint['username'],
            'pass' => $endpoint['password'],
            'host' => $endpoint['host'],
            'port' => $endpoint['port']
        );
    }
}